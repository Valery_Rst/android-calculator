package com.example.androidcalculator.additionally;

public class Constants {

    public static final String TAG_VALUE_F = "first";
    public static final String TAG_VALUE_S = "second";
    public static final String TAG_OPERATION = "operation";
    public static final String TAG_RESULT = "result";
}
