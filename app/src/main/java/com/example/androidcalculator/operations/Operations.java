package com.example.androidcalculator.operations;

public class Operations {

    public static double sum(double a, double b) {
        return (a + b);
    }

    public static double multiply(double a, double b) {
        return (a * b);
    }

    public static double diff(double a, double b) {
        return (a - b);
    }

    public static double div(double a, double b) {
        return (a / b);
    }
}