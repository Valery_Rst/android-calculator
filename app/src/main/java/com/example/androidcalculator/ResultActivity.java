package com.example.androidcalculator;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.TextView;

import com.example.androidcalculator.additionally.Constants;

public class ResultActivity extends AppCompatActivity {

    private String firstValue, secondValue, operation, result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.result_operation);
        TextView textView = findViewById(R.id.result);

        firstValue = getIntent().getStringExtra(Constants.TAG_VALUE_F);
        operation = getIntent().getStringExtra(Constants.TAG_OPERATION);
        secondValue = getIntent().getStringExtra(Constants.TAG_VALUE_S);
        result = getIntent().getStringExtra(Constants.TAG_RESULT);

        setText(textView, firstValue, operation,
                secondValue, result);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("firstOperand", firstValue);
        outState.putString("operation", operation);
        outState.putString("secondOperand", secondValue);
        outState.putString("result", result);
    }

    @Override
    protected void onRestoreInstanceState(Bundle onSaveInstanceState) {
        super.onRestoreInstanceState(onSaveInstanceState);

        if (onSaveInstanceState.containsKey("firstOperand")) {
            firstValue = onSaveInstanceState.getString("firstOperand");
        }

        if (onSaveInstanceState.containsKey("operation")) {
            operation = onSaveInstanceState.getString("operation");
        }

        if (onSaveInstanceState.containsKey("secondOperand")) {
            secondValue = onSaveInstanceState.getString("secondOperand");
        }

        if (onSaveInstanceState.containsKey("result")) {
            result = onSaveInstanceState.getString("result");
        }
    }

    public void onClickBtnGoBack(View view) {
        Intent back = new Intent(this, MainActivity.class);
        startActivity(back);
    }

    /**
     * Метод предназначен для отображения текста в TextView
     *
     * @param textView элемент TextView
     * @param firstOperand первое значение
     * @param operation знак операции
     * @param secondOperand второе значение
     * @param result результат выполнения операции
     */
    private void setText(TextView textView, String firstOperand, String operation,
                         String secondOperand, String result) {

        textView.setText(String.format("%s %s %s %s %s",
                formatDoubleValue(firstOperand), operation, formatBracket(secondOperand),
                getString(R.string.equality), formatDoubleValue(result)));
    }

    /**
     * Метод возвращает значение по математическим правилам расстановки скобок
     *
     * @param strValue значение
     * @return форматированное значение
     */
    private String formatBracket(String strValue) {
        double value = (Double.parseDouble(strValue));
        return value >= 0 ? formatDoubleValue(strValue) :
                getString(R.string.left_bracket) + formatDoubleValue(strValue) +getString(R.string.right_bracket);
    }

    /**
     * Метод возвращает значение по математическим правилам чисел с плавающей точкой
     *
     * @param strValue strValue значение
     * @return форматированное значение
     */
    private String formatDoubleValue(String strValue) {
        double value = (Double.parseDouble(strValue));
        return  (value % 1.0 == 0.0) ? String.valueOf((long) value) : String.valueOf(value);
    }
}