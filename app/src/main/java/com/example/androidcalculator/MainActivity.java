package com.example.androidcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.androidcalculator.additionally.Constants;
import com.example.androidcalculator.operations.Operations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String REG_EX = "^(-)?(0|[1-9]\\d*)([.,]\\d+)?";

    private double a, b, result;
    private EditText firstValue,secondValue;
    private String operation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        firstValue = findViewById(R.id.first_operand);
        secondValue = findViewById(R.id.second_operand);

        Button btnSum = findViewById(R.id.sum_btn);
        Button btnDiff = findViewById(R.id.diff_btn);
        Button btnDiv = findViewById(R.id.div_btn);
        Button btnMultiply = findViewById(R.id.multiply_btn);

        btnSum.setOnClickListener(this);
        btnDiff.setOnClickListener(this);
        btnDiv.setOnClickListener(this);
        btnMultiply.setOnClickListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putDouble("firstOperand", a);
        outState.putDouble("secondOperand", b);
    }

    @Override
    protected void onRestoreInstanceState(Bundle onSaveInstanceState) {
        super.onRestoreInstanceState(onSaveInstanceState);

        if (onSaveInstanceState.containsKey("firstOperand")
                && onSaveInstanceState.containsKey("secondOperand")) {
            a = onSaveInstanceState.getDouble("firstOperand");
            b = onSaveInstanceState.getDouble("secondOperand");
        }
    }

    @Override
    public void onClick(View view) {
        if (isEmptyInputField(firstValue) | isIncorrectInputField(secondValue)
                | isEmptyInputField(firstValue) | isIncorrectInputField(secondValue)) {
            return;
        }
        try {
            a = Double.parseDouble(firstValue.getText().toString().trim());
            b = Double.parseDouble(secondValue.getText().toString().trim());
        } catch (NumberFormatException ex) {
            firstValue.setError(getString(R.string.error_number_format));
            secondValue.setError(getString(R.string.error_number_format));
            return;
        }

        switch (view.getId()) {
            case R.id.sum_btn:
                result = Operations.sum(a, b);
                operation = getString(R.string.sum);
                break;
            case R.id.diff_btn:
                result = Operations.diff(a, b);
                operation = getString(R.string.diff);
                break;
            case R.id.div_btn:
                if (b == 0) {
                    secondValue.setError(getString(R.string.error_second_zero));
                    return;
                }
                result = Operations.div(a, b);
                operation = getString(R.string.div);
                break;

            case R.id.multiply_btn:
                result = Operations.multiply(a, b);
                operation = getString(R.string.multiply);
                break;
            default:
                break;
        }

        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra(Constants.TAG_VALUE_F, String.valueOf(a));
        intent.putExtra(Constants.TAG_OPERATION, operation);
        intent.putExtra(Constants.TAG_VALUE_S, String.valueOf(b));
        intent.putExtra(Constants.TAG_RESULT, String.valueOf(result));
        startActivity(intent);
    }

    /**
     * Метод проверяет значение EditText на предмет пустоты
     *
     * @param value значение
     * @return true - если поле пустое, false - если поле не пустое
     */
    private boolean isEmptyInputField(EditText value) {
        if (TextUtils.isEmpty(value.getText().toString())) {
            value.setError(getString(R.string.error_no_string));
            return true;
        }
        return false;
    }

    /**
     * Метод проверяет значение EditText на предмет некорректых символов
     *
     * @param value значение
     * @return true - если поле содержит некорректные символы,
     * false - если не содержит некорректные символы
     */
    private boolean isIncorrectInputField(EditText value) {
        Pattern pattern = Pattern.compile(REG_EX);
        Matcher matcher = pattern.matcher(value.getText().toString());
        if (!matcher.find()) {
            value.setError(getString(R.string.error_incorrect));
            return true;
        }
        return false;
    }
}